package com.example.demo;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles(profiles = "test")
@SpringBootTest
@AutoConfigureMockMvc
public class AllLayersIT {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ToDoRepository toDoRepository;

    @BeforeEach
    void initDatabase() {
        if (toDoRepository.count() != 3) {
            toDoRepository.save(new ToDoEntity(1L, "Wash the dishes"));
            toDoRepository.save(
                    new ToDoEntity(2L, "Learn to test Java app", ZonedDateTime
                            .of(1999, 12, 2, 10, 25, 2, 5,
                                    ZoneOffset.UTC))
            );
            toDoRepository.save(new ToDoEntity(3L, "Make coffee"));
        }
    }

    @Test
    void whenCompleteValidId_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(put("/todos/3/complete"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.text").isString())
                .andExpect(jsonPath("$.completedAt").exists());
    }

    @Test
    void whenDeleteValidId_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(delete("/todos/2"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));

        assertFalse(toDoRepository.findById(2L).isPresent());
    }

    @Test
    void whenCompleteNotValidId_thenReturnNotFoundResponse() throws Exception {
        this.mockMvc.perform(put("/todos/1200/complete"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find todo with id 1200"));
    }

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].text").isString())
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }
}
