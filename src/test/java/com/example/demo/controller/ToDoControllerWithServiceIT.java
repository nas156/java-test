package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;


    @Test
    void whenGetOneByIdValid_thenReturnValidResponse() throws Exception {
        String text = "New todo";
        Long id = 1L;
        ToDoEntity toDoEntity = new ToDoEntity(id, text);
        when(toDoRepository.findById(anyLong()))
                .thenReturn(Optional.of(toDoEntity));

        this.mockMvc
                .perform(get("/todos/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.text").value(text))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenGetOneByIdNotValid_thenReturnIdNotFoundResponse() throws Exception {
        this.mockMvc
                .perform(get("/todos/5"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find todo with id 5"));
    }

    @Test
    void whenCompleteIdValid_thenReturnCompleted() throws Exception {
        var todo = new ToDoEntity(0L, "Test 1");
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));
        when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
            ToDoEntity arg = i.getArgument(0, ToDoEntity.class);
            Long id = arg.getId();
            if (id.equals(todo.getId())) {
                return todo;
            } else {
                return new ToDoEntity();
            }
        });

        this.mockMvc
                .perform(put("/todos/0/complete"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(0L))
                .andExpect(jsonPath("$.text").value("Test 1"))
                .andExpect(jsonPath("$.completedAt").exists());
    }

    @Test
    void whenCompleteIdNotValid_thenReturnCompleted() throws Exception {
        this.mockMvc
                .perform(put("/todos/0/complete"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("Can not find todo with id 0"));
    }

    //-------------------------------------------------------------------------------------------------------------------
    //    @Test
//    void whenGetAll_thenReturnValidResponse() throws Exception {
//        String testText = "My to do text";
//        when(toDoRepository.findAll()).thenReturn(
//                Arrays.asList(
//                        new ToDoEntity(1l, testText)
//                )
//        );
//
//        this.mockMvc
//                .perform(get("/todos"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$").isArray())
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].text").value(testText))
//                .andExpect(jsonPath("$[0].id").isNumber())
//                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
//    }


}
