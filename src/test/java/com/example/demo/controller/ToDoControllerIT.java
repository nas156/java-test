package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoService toDoService;


    @Test
    void whenUpsert_thenReturnValidResponse() throws Exception {
        String text = "New text";
        Long id = 1L;
        var toDoResponse = ToDoEntityToResponseMapper.map(new ToDoEntity(id, text));
        when(toDoService.upsert(ArgumentMatchers.any(ToDoSaveRequest.class)))
                .thenReturn(toDoResponse);

        this.mockMvc.perform(post("/todos")
                .content("{\"id\":\"1\",\"text\":\"aaa\"}")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.text").value(text));
    }

    //------------------------------------------------------------------------------------------------------------------

    //    @Test
//    void whenGetAll_thenReturnValidResponse() throws Exception {
//        String testText = "My to do text";
//        Long testId = 1L;
//        when(toDoService.getAll()).thenReturn(
//                Arrays.asList(
//                        ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
//                )
//        );
//
//        this.mockMvc
//                .perform(get("/todos"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$").isArray())
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].text").value(testText))
//                .andExpect(jsonPath("$[0].id").isNumber())
//                .andExpect(jsonPath("$[0].id").value(testId))
//                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
//    }



}
